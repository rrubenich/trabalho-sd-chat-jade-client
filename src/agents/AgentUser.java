package agents;
import java.util.ArrayList;

import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.DFService;
import jade.domain.FIPAException;



public class AgentUser extends Agent{
	
	private ArrayList<ContainerID> containers = new ArrayList<ContainerID>();
	
	@Override
	protected void setup() {
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();   
		sd.setType("AgentUser"); 
		sd.setName(getName());
		dfd.setName(getAID());
		dfd.addServices(sd);
		
		try {
			DFService.register(this,dfd);
			ReceiveMessage receiveMessageBehaviour = new ReceiveMessage(this);
			addBehaviour(receiveMessageBehaviour);
		} catch (FIPAException e) {
			System.out.println(e.getMessage());
			doDelete();
		}
		
		System.out.println("Agent Jade on: " + getLocalName());
	}

	private class ReceiveMessage extends CyclicBehaviour {
		
		public ReceiveMessage(Agent a) {
			super(a);
		}

		public void action() {
			ACLMessage  msg = myAgent.receive();
			
			// Quando a mensagem é de usuário para usuário
			if(msg.getPerformative() == ACLMessage.PROPAGATE){

				if(msg != null){
					System.out.println("Mensagem recebida");
					System.out.println("Usuário:" + msg.getSender());
					System.out.println("Tipo:" + msg.getPerformative());
					System.out.println("Mensagem: " +msg.getContent());
				}
			}
			// Quando a mensagem é do servidor para usuário
			else if(msg.getPerformative()== ACLMessage.INFORM){
				containers.clear();
				// Achar um jeito de adicionar os containers na arraylist
				System.out.println(msg);
			}
		}
	}
	
	public void SendMessage(String message){
		
		ACLMessage msg = new ACLMessage(ACLMessage.PROPAGATE);

		
        for(ContainerID container : containers)
        {
            if(!container.getName().equals("MainContainer"))
            {
        		AID r=new AID("agent-name@platform",AID.ISGUID);
        		r.addAddresses(container.getAddress());
                msg.addReceiver(r);
            }
        }

        msg.setContent(message);
        send(msg);
	}
}
